<?php
namespace App\Controller;

class Controller {
    
    protected $url;
    
    public function __construct() {
        
        $this->url = "https://maps.googleapis.com/maps/api/";
        
    }
    
    public function __call($name, $arguments){
        if($arguments[0] === "textsearch"){
            
            return $this->url.$name.'/'.$arguments[0].'/json?'. http_build_query($arguments[1]).'&'. http_build_query($arguments[2]);
 
        }else if($arguments[0] === "details"){
            
            return $this->url.$name.'/'.$arguments[0].'/json?'.http_build_query($arguments[1]).'&'. http_build_query($arguments[2]).'&'. http_build_query($arguments[3]);
        
        }
    }
}
