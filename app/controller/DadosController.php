<?php

namespace App\Controller;

class DadosController extends Controller {
    
    public function todos($dados){
        
        return file_get_contents($dados);
    }
    
    public function placeId($dados){
        
        $data = file_get_contents($dados);
        
        $data = json_decode($data);
        
        return $data->results[0]->place_id;
    }
    
    public function detalhes($dados){
        
        return file_get_contents($dados);
    }
}

